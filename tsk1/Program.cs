﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamHW
{
    class Program
    {
        static IEnumerable<Int32> GenerateArray(Int32 arrayLength)
        {
            Random rndObj = new Random();
            for (int i = 0; i < arrayLength; i++)
            {
                yield return rndObj.Next(100);
            }
        }

        static void Main(string[] args)
        {
            Int32 arrayLength = 0;
            if (args == null || args.Count() == 0 )
            {
                Console.WriteLine("Enter array length: ");
                arrayLength = Convert.ToInt32(Console.ReadLine());
            }
            else
            {
                arrayLength = Convert.ToInt32(args[0]);
            }

            Int32[] numbers = GenerateArray(arrayLength).ToArray();
            Int32 iterations = 0;
            Console.WriteLine();        
            while (numbers.Count() > 1 )
            {
                numbers = numbers.Where((val, idx) => { if (idx % 2 != 0) Console.WriteLine("Number " + val + " was removed");
                                                        return idx % 2 == 0;}).ToArray();
                iterations++;
            }            
            Console.WriteLine("Number of iterations is " + iterations);
        }
    }
}
