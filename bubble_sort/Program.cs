﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bubble_sort
{
    public class a
    {
        //private int privateField = 1;
        public int field = 0;
    }
    public static class MyExt
    {
        public static int Ext(this a i)
        {
            return i.field;
        }
    }

    public class b : a { }


    interface IMyComparable 
    {
        bool Compare(object o);
    }

    class MyClass : IMyComparable
    {
        public int value { get; set; }

        public bool Compare(object Inst)
        {
            MyClass currInst = Inst as MyClass;
            //MyClass currInst;
            //try
            //{
            //    currInst = (MyClass)Inst;
            //}
            //catch (InvalidCastException e)
            //{

            //    currInst = null;
            //}
            
            if (currInst != null)
            {
                return currInst.value > this.value;
            }
            return false;
        }
        public MyClass() { }
        public MyClass(int initValue)
        {
            this.value = initValue;
        }
    }

    class MyClass2 : IMyComparable
    {
        public int value { get; set; }

        public bool Compare(object Inst)
        {
            MyClass2 currInst = Inst as MyClass2;
            //MyClass currInst;
            //try
            //{
            //    currInst = (MyClass)Inst;
            //}
            //catch (InvalidCastException e)
            //{

            //    currInst = null;
            //}

            if (currInst != null)
            {
                return currInst.value > this.value;
            }
            return false;
        }
        public MyClass2() { }
        public MyClass2(int initValue)
        {
            this.value = initValue;
        }
    }
    class Program
    {
        static void GenericBubbleSort(IMyComparable[] objArr) 
        {
            for (int i = 0; i < objArr.Length; i++)
            {
                for (int j = objArr.Length - 1; j > i; --j)
                {
                    if (objArr[j].Compare(objArr[j - 1]))
                    {
                        IMyComparable tmp = objArr[j - 1];
                        objArr[j - 1] = objArr[j];
                        objArr[j] = tmp;
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            int length = 10;
            MyClass[] myObjectsArray = new MyClass[10];

            Random randomObject = new Random();

            for (int i = 0; i < length; i++)
            {
                myObjectsArray[i] = new MyClass(randomObject.Next(100));
            }
            myObjectsArray.All(x => { Console.Write(((MyClass)x).value + " "); return true; });
            Console.WriteLine();
            GenericBubbleSort(myObjectsArray);
            myObjectsArray.All(x => { Console.Write(x.value + " "); return true; });

            int ii = 10;
            IComparable intObj = ii;
            Console.WriteLine( intObj.CompareTo(12));
        }
    }
}
