﻿using System;
//using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyEnumerator
{
    
    public  interface IEnumerable
    {
        IEnumerator GetEnumerator();
    }
    public interface IEnumerator
    {
        object Current { get; }
        bool MoveNext();
        void Reset();
        void Dispose();
    }

    public class MyEnumeratorObj : IEnumerator
    {
        public MyEnumeratorObj(MyCollection myCollection)
        {
            this.MyCollection = myCollection;
            this.position = -1;
        }

        private MyCollection MyCollection;

        private int position;

        public object Current
        {
           get { return MyCollection[position]; }
        }

        public bool MoveNext()
        {
            this.position++;
            return position < 10;
        }

        public void Reset()
        {
            position = -1;
        }

        public void Dispose()
        {
            Console.WriteLine("this is Dispose");
        }
    }

    public class MyCollection : IEnumerable
    {
        private Random MyRndObj;

        public int this[int index]    
        {
            get
            {
                return MyRndObj.Next(index + 100);
            }
        }
        public IEnumerator GetEnumerator()
        {
            return new MyEnumeratorObj(this);
        }
        public MyCollection()
        {
            MyRndObj = new Random();
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {

            MyCollection myCollect = new MyCollection();
            IEnumerator myEnum = myCollect.GetEnumerator();


            foreach (var item in myCollect)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            while (true)
            {
                Console.Write(myEnum.Current + " ");
                if (!myEnum.MoveNext())
                {
                    myEnum.Reset();
                    break;
                }
            }
        }
    }
}
