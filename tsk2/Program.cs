﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tsk2
{
    interface IStackable<T>
    {
        T Pop();
        void Push(T item);
        Int32 Count { get; }
    }

    class MyStack<T> : IStackable<T>
    {
        public MyStack()
        {
              this.stack = new Stack<T>();  
        }
        public T Pop()
        {
            return stack.Pop();
        }
        public void Push(T item)
        {
            stack.Push(item);
        }
        public Int32 Count
        {
            get { return stack.Count(); }
        }

        private Stack<T> stack;

    }

    interface IStd
    {
        void Print(object o);
        void PrintLine(object o);
        object Read();
    }
    class Std : IStd
    {
        public void Print(object o)
        {
            Console.Write(o);
        }

        public void PrintLine(object o)
        {
            Console.WriteLine(o);
        }

        public object Read()
        {
            return Console.ReadLine();
        }
    }

    class RandomNumberGenerator
    {
        public RandomNumberGenerator()
        {
            randomNumber = new Random();
        }

        Random randomNumber;

        public Int32 GetRandomNumber { get { return randomNumber.Next(10); } }
    }

    class Program
    {
        static IEnumerable<Int32> GenerateArray(Int32 arrayLength)
        {
            RandomNumberGenerator rndObj = new RandomNumberGenerator();
            for (int i = 0; i < arrayLength; i++)
            {
                yield return rndObj.GetRandomNumber;
            }
        }

        static void Main(string[] args)
        {
            IStd IO = new Std();

            List<Int32> numbers = GenerateArray(5).ToList();
            numbers[4] = 0;
            numbers.ForEach((val) => IO.Print(val + " "));
            IO.Print("-> \n");
            IStackable<Int32> previousNumberStack = new MyStack<Int32>();
            while (true)
            {
                Int32 choosenNumber = Convert.ToInt32(IO.Read());
                IO.Print(" -> ");
                if (previousNumberStack.Count == 0 && choosenNumber == 0)
                {
                    IO.Print("end");
                    break;
                }
                if (choosenNumber == 0)
                {
                    Int32 previousNumber = previousNumberStack.Pop();
                    numbers = numbers.Select((val) => val / previousNumber).ToList();
                }
                else
                {
                    previousNumberStack.Push(choosenNumber);
                    numbers = numbers.Select((val) => val * choosenNumber).ToList();                    
                }                
                numbers.ForEach((val) => IO.Print(val + " "));
                IO.Print(" -> \n");
            }            
        }
    }
}
